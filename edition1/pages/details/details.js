// pages/details/details.js
var videoData = require("../../data/data.js");
var cosData = require("../../data/cos.js");
Page({
  /**
   * 页面的初始数据
   */
  data: {
    currentVideo: "",
    scrollData: [],
    curr_id: ''
    
  },
 

  // 更新页面
  updataView:function(e){
    var id = e.currentTarget.id;
    var currentData = videoData.videoData[id - 1];
    this.setData({
      currentVideo: currentData
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取上一个页面的id
    var id = options.id;
    //console.log("详情页面的id：" + id);
    // 根据当前页面的id 判断出当前页面需要的详情数据在videoData数组里的下标[id-1]; 然后取值
    var currentData = videoData.videoData[id - 1];
    // 复制给data里的currentVideo
    this.setData({
      currentVideo: currentData
    }),
    this.setData({
      scrollData: videoData.scrollData
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})