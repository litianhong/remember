var classifyList = require("../../data/classify_data.js");
Page({
  data: {
    curr_id: '',
    ifys: []
  },
  // 初始化
  onLoad: function () {
    this.setData({
      ifys: classifyList.classifyList
    })
  },
  //跳转详情页
  url_one: function () {
    wx.navigateTo({
      url: "../cos/cos"
    })
  },
  url_two: function () {
    wx.navigateTo({
      url: "../food/food"
    })
  },
  url_three: function () {
    wx.navigateTo({
      url: "../fash/fash"
    })
  },
  url_four: function () {
    wx.navigateTo({
      url: "../home/home"
    })
  },
  url_five: function () {
    wx.navigateTo({
      url: "../digit/digit"
    })
  },
  url_six: function () {
    wx.navigateTo({
      url: "../mother/mother"
    })
  },
  //转发功能
  onShareAppMessage: function (res) {
    return {
      title: '',
      path: '/pages/classify/classify',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },
})
