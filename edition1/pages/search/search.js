// pages/index/search/search.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchStatus: false,
    his: true,
    hot: true,
    list: false,
    hisData: [],
    inputText: '',

    // 搜索列表
    active: false,
    SynActive: false,
    PayActive: false,
    ChoiceActive: false,
    NewActiveClass: false,
    choiceText: '综合',
    area: true,
    num: 1,
    i: 1,
    goodsList: [],
    goodsNum: 1
  },

  // 清除历史搜索
  onClearHisTap: function (e) {
    this.setData({
      his: false
    })
    wx.removeStorageSync("hisList");
  },
  // 关闭搜索列表
  onCloseListTap: function (e) {
    this.setData({
      his: true,
      hot: true,
      list: false,
      searchStatus: false,
      inputText: '',
    })
  },
  // 输入框输入文字时
  onbindInput: function (e) {
    this.setData({
      his: false,
      hot: false,
      list: true,
      searchStatus: true,
      inputText: e.detail.value,
      area: false,
    })
    console.log(this.data.hisData, e.detail.value);
    if (e.detail.value == "") {
      this.setData({
        his: true,
        hot: true,
        list: false,
        searchStatus: false,
        area: false,
      })
      if (this.data.hisData.length == 0) {
        console.log(this.data.his);
        this.setData({
          his: false,
          hot: true,
          list: false,
          searchStatus: false,
          area: false,
        })
      }
    }
  },
  // 输入框获取焦点时
  onFocusInput: function (e) {
    if (e.detail.value != "") {
      this.setData({
        his: false,
        hot: false,
        list: true,
        searchStatus: true,
        area: false,
      })
    }
  },
  // 点击搜索按钮
  toSearchInput: function (e) {
    console.log(this.data.inputText);
    if (this.data.searchStatus) {
      var val = this.data.inputText;
      this.setData({
        area: true,
        list: false,
        goodsList: []
      });
      this.getData("", val, "");
      this.getStroage(val);
    } else {
      wx.navigateBack({ changed: true });
    }
  },
  // 点击热门按钮搜索
  toSearchGoods: function (e) {
    var val = e.currentTarget.dataset.val
    this.setData({
      inputText: val,
      area: true
    });
    this.getData("", val, "");
    this.getStroage(val);
  },

  // 点击热门搜索换一批
  toHuan: function () {
    this.setData({
      i: this.data.i++
    })
    var data = {
      cur: this.data.i,
      rp: 10
    }
    console.log(data);
    var url = app.globalData.baseUrl + "online/goods/popular";
    var that = this;
    wx.request({
      url: url,
      data: data,
      header: app.globalData.header(),
      method: 'POST',
      success: function (res) {
        console.log(res.data)
      },
      fail: function (res) {
        console.log(res.msg)
      }
    })
  },
  // 获取数据
  getData: function (id, val, data) {
    this.setData({
      goodsList: []
    })
    var that = this;
    var url = app.globalData.baseUrl + 'online/goods/search';
    var data = data || {
      searchText: val,
      gcId: id,
      orderType: 1,
      beginPrice: "",
      endPrice: '',
      cur: 1,
      rp: 10
    };
    //console.log(data);
    wx.request({
      url: url, //仅为示例，并非真实的接口地址
      data: data,
      header: app.globalData.header(),
      method: 'POST',
      success: function (res) {
        console.log(res.data)
        var goodsData = res.data.obj
        var total = goodsData.total;//总条数
        var totalNum = Math.ceil(total / 10);//总页数
        var newGoodsData = that.data.goodsList.concat(goodsData.rows)//合并数组
        //console.log(data);
        //更新数组
        that.setData({
          goodsList: newGoodsData,
          totalNum: totalNum,
          loadShow: true
        })
        wx.hideLoading();
      },
      fail: function (res) {
        console.log(res.msg)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    var val = options.val;
    this.setData({
      SynActiveClass: true,
      loadShow: false
    })
    wx.showLoading({
      title: '加载中',
    })
    var url = app.globalData.baseUrl + "/online/goods/search";
    if (id || val) {
      this.setData({
        list: false,
        inputText: val,
      });
      this.getData(id, "", "");
      if (val == 0) {
        this.setData({
          inputText: '',
        });
        this.getData(id, "", "");
      }
    } else {
      this.setData({
        area: false
      })
      wx.hideLoading();
    }
    this.getStroage();
  },

  // 本地缓存历史搜搜
  getStroage: function (val) {
    // 获取缓存中的历史搜索
    var hisList = wx.getStorageSync('hisList') || [];
    //console.log(hisList);
    if (!hisList[0]) {
      this.setData({
        his: false,
      });
    }
    console.log(this.data.his);
    if (val) {
      hisList.push(val);
      wx.setStorageSync("hisList", hisList);
    }
    this.setData({
      hisData: hisList
    })
  },
  //获取新数据
  getNewData: function (url, data) {
    var that = this;
    wx.request({
      url: url, //仅为示例，并非真实的接口地址
      data: data,
      header: app.globalData.header(),
      method: 'POST',
      success: function (res) {
        // console.log(res.data)
        var goodsData = res.data.obj;
        var total = goodsData.total;//总条数
        var totalNum = Math.ceil(total / 10);//总页数
        var newGoodsData = that.data.goodsList.concat(goodsData.rows)//合并数组
        //更新数组
        that.setData({
          goodsList: newGoodsData,
          totalNum: totalNum
        })
      },
      fail: function (res) {
        console.log(res.msg)
      }
    })
  },
  // 上滑加载
  onReachBottom: function () {
    console.log("上滑加载")
    var goodsNum = this.data.goodsNum;//最多加载的次数
    var newGoodsList = [];//新建一个数组用来保存此次获取到的新数据
    var url = app.globalData.baseUrl + 'online/goods/popular';
    this.setData({
      goodsNum: goodsNum + 1
    })
    //console.log(this.data.goodsNum)
    var data = {
      cur: this.data.goodsNum,
      rp: 10
    }
    var data = {
      searchText: this.data.val,
      gcId: '',
      orderType: this.data.orderType,
      beginPrice: "",
      endPrice: '',
      cur: 1,
      rp: 10
    };
    if (goodsNum < this.data.totalNum) {
      this.getNewData(url, data)
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})