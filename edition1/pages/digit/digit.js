var videoData = require("../../data/digit.js");
Page({
  data: {
    curr_id: '',
    items: [],
    pullNum: 1,
    loadShow: true
  },
  //获取新的数据
  getNewList: function (cur) {
    var newStart = cur * 5 - 5;
    var newEnd = cur * 5;
    var loadList = videoData.videoData.slice(newStart, newEnd)
    console.log(loadList);
    var nowList = this.data.items.concat(loadList);

    this.setData({
      items: nowList
    })

  },
  // 初始化
  onLoad: function () {
    var loadList = videoData.videoData.slice(0, 5)
    console.log('长度：' + videoData.videoData.length)
    this.setData({
      items: loadList
    });
  },
  onReachBottom: function () {
    console.log("上拉加载")
    var pullNum = this.data.pullNum;
    var newItems = [];
    this.setData({
      pullNum: pullNum + 1
    })
    this.getNewList(this.data.pullNum)


    var totalNum = videoData.videoData.length / 5;
    console.log(totalNum, pullNum);
    if (pullNum >= totalNum) {
      this.setData({
        loadShow: false
      })
    }

  },
  //转发功能
  onShareAppMessage: function (res) {
    return {
      title: '',
      path: '/pages/digit/digit',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },

  //列表详情页跳转
  newUrl: function (u) {
    var that = this;
    var id = u.target.id;
    wx.navigateTo({
      url: "../details/details?id=" + id
    })


  },
  // 视频展开收起按钮
  closeOrOpen: function (e) {
    var that = this;
    var id = e.target.id;
    for (var i = 0; i < that.data.items.length; i++) {
      if (id == that.data.items[i].id) {
        var isFold = 'items[' + i + '].isFold';
        var Fold = that.data.items[i].isFold;
        that.setData({
          [isFold]: !Fold
        })
      }
    }
  },
  onReady: function () {
    this.videoContext = wx.createVideoContext('myVideo')
  },
  videoPlay(e) {
    this.setData({
      curr_id: e.currentTarget.dataset.id,
    })
    this.videoContext.play()
  },

})
