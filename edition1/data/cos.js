//分类列表美妆个护
var videoData = [
  {
    isFold: false,
    id: 1,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558953536830&di=1721cdce2c9aa746631d060e6a5a1949&imgtype=0&src=http%3A%2F%2Fimg.pconline.com.cn%2Fimages%2Fupload%2Fupc%2Ftx%2Fphotoblog%2F1111%2F02%2Fc2%2F9483142_9483142_1320214187557_mthumb.jpg',
    video_title: "我们在庐山\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 2,
    src: 'http://p33rvbggj.bkt.clouddn.com/heitouyi_one_zip',
    poster: 'http://img.pconline.com.cn/images/upload/upc/tx/photoblog/1212/11/c4/16513587_16513587_1355202142984.jpg',
    video_title: "我们在三清山\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 3,
    src: 'http://p33rvbggj.bkt.clouddn.com/ganweidaoTwo_one_zip',
    poster: 'http://pic.lvmama.com/uploads/pc/place2/2016-05-14/32ffc3f4-c684-4fef-b3b7-72d2ac9b04c6.jpg',
    video_title: "我们在东极岛！\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 4,
    src: 'http://p33rvbggj.bkt.clouddn.com/miandangjiamiss_one_zip',
    poster: 'http://img8.zol.com.cn/bbs/upload/14694/14693673.jpg',
    video_title: "我们在厦门植物园\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  }
]
module.exports = {
  videoData: videoData
}