var videoData = [
  {
    isFold: false,
    id: 14,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'http://n4-q.mafengwo.net/s8/M00/E1/FD/wKgBpVYaF8eAQt3hAA5zrdvKMaE62.jpeg?imageView2/2/w/680/q/90',
    video_title: "广州点都德\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  }
]
module.exports = {
  videoData: videoData
}