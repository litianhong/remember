var videoData = [
  {
    isFold: false,
    id: 5,
    src: 'http://p33rvbggj.bkt.clouddn.com/shanchayou_one_zip',
    poster: 'http://hiphotos.baidu.com/lvpics/pic/item/f3d3572c11dfa9ecea82aa5d62d0f703908fc1c8.jpg',
    video_title: "我们在西塘啃猪脚\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 6,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'http://img-arch.pconline.com.cn/images/upload/upc/tx/photoblog/1510/14/c6/13936351_13936351_1444747194569.jpg',
    video_title: "在平江路不给小猪买车车\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  }
]
module.exports = {
  videoData: videoData
}