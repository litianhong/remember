//分类列表数据
var classifyList = [
  {
    id : 1,
    detaUrl : "url_one",
    src_left: "http://gss0.baidu.com/-fo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/58ee3d6d55fbb2fbe514fd774f4a20a44623dc9c.jpg",
    video_number_left : "4个视频",
    classify_name_left : "游山玩水",
    refresh_time_left : "2019-05-27\t更新"
  },
  {
    id: 2,
    detaUrl: "url_two",
    src_left: "http://gss0.baidu.com/-fo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/58ee3d6d55fbb2fbe514fd774f4a20a44623dc9c.jpg",
    video_number_left: "2个视频",
    classify_name_left: "江南小镇",
    refresh_time_left : "2019-05-27\t更新"
  },
  {
    id: 3,
    detaUrl: "url_three",
    src_left: "http://gss0.baidu.com/-fo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/58ee3d6d55fbb2fbe514fd774f4a20a44623dc9c.jpg",
    video_number_left: "7个视频",
    classify_name_left: "都市风光",
    refresh_time_left : "2019-05-27\t更新"
  },
  {
    id: 4,
    detaUrl: "url_four",
    src_left: "http://gss0.baidu.com/-fo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/58ee3d6d55fbb2fbe514fd774f4a20a44623dc9c.jpg",
    video_number_left: "1个视频",
    classify_name_left: "美食攻略",
    refresh_time_left : "2019-05-27\t更新"
  },
  {
    id: 5,
    detaUrl: "url_five",
    src_left: "http://gss0.baidu.com/-fo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/58ee3d6d55fbb2fbe514fd774f4a20a44623dc9c.jpg",
    video_number_left: "3个视频",
    classify_name_left: "旅游打卡",
    refresh_time_left : "2019-05-27\t更新"
  },
  {
    id: 6,
    detaUrl: "url_six",
    src_left: "http://gss0.baidu.com/-fo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/58ee3d6d55fbb2fbe514fd774f4a20a44623dc9c.jpg",
    video_number_left: "2个视频",
    classify_name_left: "生活日常",
    refresh_time_left : "2019-05-27\t更新"
  }
]
module.exports = {
  classifyList: classifyList
}