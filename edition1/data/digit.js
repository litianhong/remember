var videoData = [
  {
    isFold: false,
    id: 15,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'http://img.mp.itc.cn/upload/20170718/ba371025f4c440e2994e2c8b0f77de31_th.jpg',
    video_title: "长隆野生动物世界，哈哈哈小猪得到一个小脑斧\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 16,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'http://img5.pcpop.com/ArticleImages/0x0/4/4116/004116597.jpg',
    video_title: "广州小蛮腰\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 17,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'http://img.pconline.com.cn/images/upload/upc/tx/itbbs/1703/06/c0/38752826_1488731560863_mthumb.jpg',
    video_title: "深圳湾公园\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  }
]
module.exports = {
  videoData: videoData
}