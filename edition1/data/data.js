//首页视频列表数据
var videoData = [
  {
    isFold: false,
    id: 1,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558953536830&di=1721cdce2c9aa746631d060e6a5a1949&imgtype=0&src=http%3A%2F%2Fimg.pconline.com.cn%2Fimages%2Fupload%2Fupc%2Ftx%2Fphotoblog%2F1111%2F02%2Fc2%2F9483142_9483142_1320214187557_mthumb.jpg',
    video_title: "我们在庐山\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 2,
    src: 'http://p33rvbggj.bkt.clouddn.com/heitouyi_one_zip',
    poster: 'http://img.pconline.com.cn/images/upload/upc/tx/photoblog/1212/11/c4/16513587_16513587_1355202142984.jpg',
    video_title: "我们在三清山\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 3,
    src: 'http://p33rvbggj.bkt.clouddn.com/ganweidaoTwo_one_zip',
    poster: 'http://pic.lvmama.com/uploads/pc/place2/2016-05-14/32ffc3f4-c684-4fef-b3b7-72d2ac9b04c6.jpg',
    video_title: "我们在东极岛！\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 4,
    src: 'http://p33rvbggj.bkt.clouddn.com/miandangjiamiss_one_zip',
    poster: 'http://img8.zol.com.cn/bbs/upload/14694/14693673.jpg',
    video_title: "我们在厦门植物园\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 5,
    src: 'http://p33rvbggj.bkt.clouddn.com/shanchayou_one_zip',
    poster: 'http://hiphotos.baidu.com/lvpics/pic/item/f3d3572c11dfa9ecea82aa5d62d0f703908fc1c8.jpg',
    video_title: "我们在西塘啃猪脚\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 6,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'http://img-arch.pconline.com.cn/images/upload/upc/tx/photoblog/1510/14/c6/13936351_13936351_1444747194569.jpg',
    video_title: "在平江路不给小猪买车车\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 7,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'http://www.ntjoy.com/liv_loadfile/news/tckx/fold450/1513071270_57150900.jpg',
    video_title: "首先当然是南昌啦\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 8,
    src: 'http://p33rvbggj.bkt.clouddn.com/heitouyi_one_zip',
    poster: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558963237939&di=3ff5f395619b7361cb090b4932d4842b&imgtype=0&src=http%3A%2F%2Fwww.gotohz.com%2Fsy_10708%2Fxwdt%2F201611%2FW020161122800976191524.jpg',
    video_title: "后来去了杭州\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 9,
    src: 'http://p33rvbggj.bkt.clouddn.com/ganweidaoTwo_one_zip',
    poster: 'http://s4.sinaimg.cn/bmiddle/51dbc5fc4510bf01e3643',
    video_title: "上有天堂，下有苏杭！苏州\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 10,
    src: 'http://p33rvbggj.bkt.clouddn.com/miandangjiamiss_one_zip',
    poster: 'http://img.pconline.com.cn/images/upload/upc/tx/photoblog/1310/01/c30/26717732_26717732_1380628893750_mthumb.jpg',
    video_title: "去了福州！\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 11,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'http://img.pconline.com.cn/images/upload/upc/tx/itbbs/1601/22/c28/17817792_1453449097652_mthumb.jpg',
    video_title: "去了厦门！\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 12,
    src: 'http://p33rvbggj.bkt.clouddn.com/heitouyi_one_zip',
    poster: 'http://pic.qjimage.com/ph113/high/ph2197-p01878.jpg',
    video_title: "去了深圳！\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 13,
    src: 'http://p33rvbggj.bkt.clouddn.com/ganweidaoTwo_one_zip',
    poster: 'http://img-arch.pconline.com.cn/images/upload/upc/tx/photoblog/1404/03/c1/32747685_32747685_1396490767317.jpg',
    video_title: "去了广州！\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 14,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'http://n4-q.mafengwo.net/s8/M00/E1/FD/wKgBpVYaF8eAQt3hAA5zrdvKMaE62.jpeg?imageView2/2/w/680/q/90',
    video_title: "广州点都德\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 15,
    src: 'https://fridaylee.oss-cn-beijing.aliyuncs.com/file/%E9%95%BF%E9%9A%86%E5%8A%A8%E7%89%A9%E5%9B%AD%E7%89%87%E6%AE%B5.mp4',
    poster: 'http://img.mp.itc.cn/upload/20170718/ba371025f4c440e2994e2c8b0f77de31_th.jpg',
    video_title: "长隆野生动物世界，哈哈哈小猪得到一个小脑斧\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 16,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'http://img5.pcpop.com/ArticleImages/0x0/4/4116/004116597.jpg',
    video_title: "广州小蛮腰\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 17,
    src: 'http://p33rvbggj.bkt.clouddn.com/guocumianmo_one_zip',
    poster: 'http://img.pconline.com.cn/images/upload/upc/tx/itbbs/1703/06/c0/38752826_1488731560863_mthumb.jpg',
    video_title: "深圳湾公园\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 18,
    src: 'http://p33rvbggj.bkt.clouddn.com/ganweidaoTwo_one_zip',
    poster: 'http://img.juimg.com/tuku/yulantu/140121/328269-140121154Z142.jpg',
    video_title: "璐璐过生日！逐渐暴躁\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  },
  {
    isFold: false,
    id: 19,
    src: 'http://p33rvbggj.bkt.clouddn.com/miandangjiamiss_one_zip',
    poster: 'http://img.juimg.com/tuku/yulantu/140121/328269-140121154Z142.jpg',
    video_title: "两周年快乐！\n",
    video_content: "你看过了许多美景，你看过了许多美女。你迷失在地图上每一道短暂的光阴，你品尝了夜的巴黎，你踏过下雪的北京，你熟记书本里每一句你最爱的真理，却说不出你爱我的原因，却说不出你欣赏我哪一种表情，却说不出在什么场合我曾让你动心，说不出离开的原因，你累积了许多飞行。\n",
    video_time: "2019.05.27 第1次发布",
    scroll_text: "为你优选"
  }
]

//详情页底部导航
var scrollData = [
  {
    id: 1,
    scroll_src: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558958369771&di=6789d8db593dfa9ba3d69c2be0484cf1&imgtype=0&src=http%3A%2F%2Fimg.alicdn.com%2Fimgextra%2Fi2%2F752147982%2FTB2udRPXkRAt1Jjy1XdXXc_kFXa_%2521%2521752147982.jpg",
    scroll_bottom_title: "我们马上要去 - 台湾",
  },
  {
    id: 0,
    scroll_src: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558958417260&di=12e7c15588306a87cf4523f623d15b38&imgtype=0&src=http%3A%2F%2Fimg.pconline.com.cn%2Fimages%2Fupload%2Fupc%2Ftx%2Fphotoblog%2F1702%2F04%2Fc7%2F36668881_1486186806053.jpg",
    scroll_bottom_title: "我们马上要去 - 泰国"
  },
  {
    id: 0,
    scroll_src: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558958444967&di=c5c3685f0e2d05ef72ffe8536c68b4a5&imgtype=0&src=http%3A%2F%2Fgss0.baidu.com%2F94o3dSag_xI4khGko9WTAnF6hhy%2Flvpics%2Fh%3D800%2Fsign%3D9857f18c0bf79052f01f4a3e3cf1d738%2F5882b2b7d0a20cf4895e7c4074094b36adaf9967.jpg",
    scroll_bottom_title: "我们马上要去 - 云南"
  },
  {
    id: 0,
    scroll_src: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558958471976&di=9ebfc57a6d25977a848897097401ebe9&imgtype=0&src=http%3A%2F%2Fimg.pconline.com.cn%2Fimages%2Fupload%2Fupc%2Ftx%2Fphotoblog%2F1309%2F13%2Fc9%2F25651122_25651122_1379082787408.jpg",
    scroll_bottom_title: "我们马上要去 - 青海"
  },
  {
    id: 0,
    scroll_src: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1558958519936&di=fdb460159447a8bf97a34996feb100ff&imgtype=0&src=http%3A%2F%2Fimg.pconline.com.cn%2Fimages%2Fupload%2Fupc%2Ftx%2Fitbbs%2F1504%2F29%2Fc10%2F6099850_1430266926025.jpg",
    scroll_bottom_title: "我们马上要去 - 海南"
  }
]

module.exports = {
  videoData: videoData,
  scrollData: scrollData,
}